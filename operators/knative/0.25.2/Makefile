# HELP
# This will output the help for each task
# thanks to https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
.PHONY: help

help: ## This help.
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

.DEFAULT_GOAL := help

SHELL := /bin/bash

CI_REGISTRY=registry.gitlab.com/fastplatform/ops/kustomize-bases

FASTPLATFORM_OPS_KUSTOMIZE_BASES_KNATIVE_REGISTRY=registry.gitlab.com/fastplatform/ops/kustomize-bases/knative

KNATIVE_NET_CERTMANAGER_REGISTRY=gcr.io/knative-releases/knative.dev/net-certmanager/cmd 
KNATIVE_NET_ISTIO_REGISTRY=gcr.io/knative-releases/knative.dev/net-istio/cmd 
KNATIVE_OPERATOR_REGISTRY=gcr.io/knative-releases/knative.dev/operator/cmd
KNATIVE_SERVING_REGISTRY=gcr.io/knative-releases/knative.dev/serving/cmd


## Image mirroring

# Mirror Docker images
.PHONY: mirror-docker-images
mirror-docker-images: ## Mirror Docker images
	$(eval SOURCE_FASTPLATFORM_OPS_KUSTOMIZE_BASES_KNATIVE_REGISTRY := $(FASTPLATFORM_OPS_KUSTOMIZE_BASES_KNATIVE_REGISTRY))
	$(eval SOURCE_KNATIVE_NET_CERTMANAGER_REGISTRY := $(KNATIVE_NET_CERTMANAGER_REGISTRY))
	$(eval SOURCE_KNATIVE_NET_ISTIO_REGISTRY := $(KNATIVE_NET_ISTIO_REGISTRY))
	$(eval SOURCE_KNATIVE_OPERATOR_REGISTRY := $(KNATIVE_OPERATOR_REGISTRY))
	$(eval SOURCE_KNATIVE_SERVING_REGISTRY := $(KNATIVE_SERVING_REGISTRY))
	$(eval TARGET_REGISTRY := $(CI_REGISTRY))
	@cat deployment.yaml | \
	  grep ${CI_REGISTRY}/knative/ | \
	  sed 's,.*${CI_REGISTRY}\(.*\),${TARGET_REGISTRY}\1,g' | \
	  sort | uniq | \
	  xargs -n 1 sh -c '\
		docker pull $$(echo $$0 | sed 's,${TARGET_REGISTRY}/knative,${SOURCE_FASTPLATFORM_OPS_KUSTOMIZE_BASES_KNATIVE_REGISTRY},g') && \
		docker tag $$(echo $$0 | sed 's,${TARGET_REGISTRY}/knative,${SOURCE_FASTPLATFORM_OPS_KUSTOMIZE_BASES_KNATIVE_REGISTRY},g') $$(echo $$0 | sed 's,@sha256,,g') && \
		docker push $$(echo $$0 | sed 's,@sha256,,g')'
	source scripts/mirror-registry.sh && \
	  mirror \
	    ${SOURCE_KNATIVE_NET_CERTMANAGER_REGISTRY} \
	    ${TARGET_REGISTRY}/knative-net-certmanager \
	    "(controller webhook)" \
	    "(v0.25.1)" && \
	  mirror \
	    ${SOURCE_KNATIVE_NET_ISTIO_REGISTRY} \
	    ${TARGET_REGISTRY}/knative-net-istio \
	    "(controller webhook)" \
	    "(v0.25.1)" && \
	  mirror \
	    ${SOURCE_KNATIVE_OPERATOR_REGISTRY} \
	    ${TARGET_REGISTRY}/knative \
	    "(operator)" \
	    "(v0.25.2)" && \
	  mirror \
	    ${SOURCE_KNATIVE_SERVING_REGISTRY} \
	    ${TARGET_REGISTRY}/knative \
	    "(activator autoscaler autoscaler-hpa controller domain-mapping domain-mapping-webhook queue webhook)" \
	    "(v0.25.1)"
