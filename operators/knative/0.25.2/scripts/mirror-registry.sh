#!/bin/sh

function mirror {
    source_hub=$1
    dest_hub=$2 # Replace this with the destination hub
    images=$3 # images to mirror.
    versions=$4 # versions to copy

    eval images="${images[@]}"
    eval versions="${versions[@]}"

    for image in "${images[@]}"; do
        for version in "${versions[@]}"; do
            name=$image:$version
            docker pull $source_hub/$name
            docker tag $source_hub/$name $dest_hub/$name
            docker push $dest_hub/$name
        done
    done

    eval images=()
    eval versions=()
}
