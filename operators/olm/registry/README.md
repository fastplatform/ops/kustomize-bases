# Operator Lifecycle Manager

[Operator Lifecycle Manager](https://github.com/operator-framework/operator-lifecycle-manager) (OLM) extends Kubernetes to provide a declarative way to install, manage, and upgrade Operators and their dependencies in a cluster.

OLM is used in the FaST Platform to manage the following operators:
- [cert-manager](https://github.com/jetstack/cert-manager)
- [grafana](https://github.com/grafana-operator/grafana-operator)
- [postgresql](https://github.com/CrunchyData/postgres-operator)
- [prometheus](https://github.com/prometheus-operator/prometheus-operator)

This repository includes all the assets to build and publish an ```OLM registry``` for FaST Platform.

All the operator manifests bundled in this registry come from the official [upstream-community-operators](https://github.com/operator-framework/community-operators/tree/master/upstream-community-operators) maintained by [RedHat Openhift](https://docs.openshift.com/container-platform/4.1/applications/operators/olm-understanding-olm.html).

## Process to update the OLM registry with a new version of the cert-manager operator

1. Copy the manifests of the new version of the Operator from the [upstream-community-operators](https://github.com/k8s-operatorhub/community-operators/tree/master//operators/cert-manager) repository in ```manifests/cert-manager``` directory.

2. Replace each occurence of the cert-manager registry (```quay.io```) by the private registry of the FaST Platform.

3. Mirror all the cert-manager Docker images
```bash
make TARGET_REGISTRY=registry.gitlab.com/fastplatform-<region>/ops/kustomize-bases mirror-cert-manager-docker-images
```

4. Build the OLM registry Docker image
```bash
make TARGET_REGISTRY=registry.gitlab.com/fastplatform-<region>/ops/kustomize-bases build
```

5. Push the OLM registry Docker image
```bash
make TARGET_REGISTRY=registry.gitlab.com/fastplatform-<region>/ops/kustomize-bases push
```

## Process to update the OLM registry with a new version of the grafana operator

1. Copy the manifests of the new version of the Operator from the [upstream-community-operators](https://github.com/k8s-operatorhub/community-operators/tree/master//operators/grafana-operator) repository in ```manifests/grafana``` directory.

2. Replace each occurence of the grafana registries (```quay.io```, ```gcr.io```) by the private registry of the FaST Platform.

3. Mirror all the grafana Docker images
```bash
make TARGET_REGISTRY=registry.gitlab.com/fastplatform-<region>/ops/kustomize-bases mirror-grafana-docker-image
```

4. Build the OLM registry Docker image
```bash
make TARGET_REGISTRY=registry.gitlab.com/fastplatform-<region>/ops/kustomize-bases build
```

5. Push the OLM registry Docker image
```bash
make TARGET_REGISTRY=registry.gitlab.com/fastplatform-<region>/ops/kustomize-bases push
```

## Update the OLM registry with a new version of the postgresql Operator

1. Copy the manifests of the new version of the Operator from the [upstream-community-operators](https://github.com/k8s-operatorhub/community-operators/tree/master/operators/postgresql) repository in ```manifests/postgresql``` directory.

2. Comment all the ```RELATED_IMAGE_*```environment variables in the postgresoperator.v[VERSION].clusterserviceversion.yaml manifest (cf. Issue [#2528](https://github.com/CrunchyData/postgres-operator/issues/2528))

3. Replace each occurence of the CrunchyData registry (```registry.developers.crunchydata.com```) by the private registry of the FaST Platform.

4. Mirror all the Crunchy Container Suite (Docker images)
```bash
make TARGET_REGISTRY=registry.gitlab.com/fastplatform-<region>/ops/kustomize-bases mirror-postgresql-docker-images
```

5. Build the OLM registry Docker image
```bash
make TARGET_REGISTRY=registry.gitlab.com/fastplatform-<region>/ops/kustomize-bases build
```

5. Push the OLM registry Docker image
```bash
make TARGET_REGISTRY=registry.gitlab.com/fastplatform-<region>/ops/kustomize-bases push
```

## Process to update the OLM registry with a new version of the prometheus operator

1. Copy the manifests of the new version of the Operator from the [upstream-community-operators](https://github.com/k8s-operatorhub/community-operators/tree/master//operators/prometheus) repository in ```manifests/prometheus``` directory.

2. Replace each occurence of the prometheus registry (```quay.io```) by the private registry of the FaST Platform.

3. Mirror all the prometheus Docker images
```bash
make TARGET_REGISTRY=registry.gitlab.com/fastplatform-<region>/ops/kustomize-bases mirror-prometheus-docker-images
```

4. Build the Registry Docker image
```bash
make TARGET_REGISTRY=registry.gitlab.com/fastplatform-<region>/ops/kustomize-bases build
```

5. Push the Registry Docker image
```bash
make TARGET_REGISTRY=registry.gitlab.com/fastplatform-<region>/ops/kustomize-bases push
