# Kustomize bases

This repository contains a set of generic Kubernetes manifests to deploy the prerequisite components needed to orchestrate the FaST services such as Istio, Knative, Crunchy Data PostgreSQL Operator, cert-manager, etc ....

These generic manifests are intended to be customized to match the specifics of each participating Member State. This customization must be done with [Kustomize](https://kustomize.io/), a Kubernetes native
configuration management tool.
Only the version ```3.8.x``` of Kustomize has been tested and validated.

Best practice is to [vendor](https://stackoverflow.com/questions/26217488/what-is-vendoring) this library of generic manifests in the repositories that use it.

## Build ready-to-use Kubernetes manifests

Use Kustomize to get a ready-to-use Kubernetes manifest:
```bash
kustomize build <path/to/a/base/manifsts>
```

Example for ```gitlab-runner```:
```bash
kustomize build gitlab/gitlab-runner/14.6.0
```

## Publish Docker images

The generic Kubernetes manifests in this repository contain references to existing public Docker images.

These Docker images can be mirror in a private repository with the below command so that each participating Member State can have its own copy.

```bash
make TARGET_REGISTRY=registry.gitlab.com/fastplatform-<region>/ops/kustomize-bases publish
```
